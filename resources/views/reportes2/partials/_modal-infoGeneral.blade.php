<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Reporte General</h4>
			</div>
			<div class="modal-body">
				<p>Este reporte arrojara los datos de busqueda agrupados por mes, contando los rango <strong>desde la fecha de inicio hasta la fecha final seleccionada</strong>, es decir, si marcas el <strong>15 de Septiembre</strong> hasta
					el <strong>16 de Octubre</strong> la tabla agrupara los resultados por mes contando desde el 15 de septiembre hasta el 30 de septiembre y comenzara de nuevo desde el 1 de Octubre hasta el 16 del mismo mes.</p>

				<p>Aclarar tambien que los resultados se basan en el ultimo movimiento de cada encomienda, es decir, el ultimo transito registrado.</p>

				<p>Los resultados individuales podran obtenerce al enlazar cualquiera de
					los meses en la tabla de resultados.</p>						</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->